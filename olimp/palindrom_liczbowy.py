'''
Created on Feb 21, 2018
Sprawdzenie czy liczba x jest  palindromem.
Jesli nie, to sprawdzenie hipotezy, ze zadziala algorytm odwrocenie liczby, dodanie jej do oryginalnej, sprawdzenia i powtarzania do oporu.
@author: obockows
'''


def is_palindrom(x):
    dlugosc = len(x)
    dlugosc = int(dlugosc)
    x = int(x)
    if (dlugosc == 1):
        return True
    else:
        pierwsza = x / (10 ** (dlugosc - 1))
        druga = x % (10)
        if pierwsza == druga:
            y = (x - (pierwsza * (10 ** (dlugosc - 1)))) / 10
            y = str(y)
            return is_palindrom(y)
        else:
            return False


def is_theory_true(x, n, count=0):
    if (is_palindrom(x)):
        print "Po zastosowaniu teorii liczba {liczba} jest palindromem po {ile} probach.".format(liczba=x, ile=count)
        return True
    else:
        temp = x[::-1]
        temp = int(temp)
        temp = temp + int(x)
        temp = str(temp)
        print "Liczba {liczba} nie jest palindromem, ale sprobujmy zastosowac teorie i sprawdzmy dla {nowa}.".format(liczba=x, nowa=temp)
        if n == 0:
            return "Nie ma sensu dalej, przerwano po %s probach." % count
        return is_theory_true(temp, n - 1, count + 1)

        
for liczba in range(1, 201):
    print "LICZBA:{0:>10}".format(liczba)
    liczba = str(liczba)
    wynik1 = is_palindrom(liczba)
    if wynik1:
        print "liczba {0} jest palindromem: {1}".format(liczba, wynik1)
    else:
        wynik2 = is_theory_true(liczba, 100)
        print "po zastosowaniu teorii czy liczba {0} jest palindromem: {1}".format(liczba, wynik2)
    print "*"*50
