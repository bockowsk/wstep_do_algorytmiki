'''
Created on Feb 28, 2018

@author: obockows
uklad rownan liniowy
'''

import numpy

while True:
    print 'uklad rownan: Ax+By=C Dx+Ey=F, podaj A, B, C, D, E, F'
    podane = raw_input("podaj A B C D E F: ").split();
    if len(podane) != 6:
        print 'podaj 6 liczby'
        continue
    else:
        a, b, c, d, e, f = podane
    try:
        a = float(a);b = float(b);c = float(c);d = float(d);e = float(e);f = float(f)
        print 'Podane liczby:' + '\t%-5f' * 6 % (a, b, c, d, e, f)
        break
    except:
        print 'podaj liczby!'

if a / d == b / e == c / f:
    print "Uklad nieoznaczony, nieskonczenie wiele rozwiazan."
elif a / d == b / e != c / f:
    print "Uklad sprzeczny, wiec nie ma rozwiazan"
else:
    macierz_wspolczynnikow = numpy.array([[a, b], [d, e]])
    macierz_wynikow = numpy.array([c, f])
    wynik = numpy.linalg.solve(macierz_wspolczynnikow, macierz_wynikow)
    print "Uklad oznaczony, rozwiazanie to:"
    print wynik
