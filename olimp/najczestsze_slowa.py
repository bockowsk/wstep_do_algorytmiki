'''
Created on Feb 19, 2018

@author: obockows
'''
import codecs

plik = codecs.open("../pan-tadeusz.txt", mode="r", encoding="utf8")

slowa = {}
# robione celowo bez regexp
znaki_specjalne = '[.,\/#!$%^&*;:?{}=-_`~/()'
lista_znakow_interpunkcyjnych = list(znaki_specjalne)
# robione celowo w celu nauki unicode
"""
przykladowy output (w bitach)
0000030: 01100001 00111010 00001101 00001010 11000010 10101011  a:....
0000036: 01010000 01110010 01100001 01110111 01100100 01100001  Prawda
specjalny znak to 2 Bajty: 11000010 10101011
czyli 0x000010101011 co rowna sie: 0xAB, czyli 0xC2 AB w hexdump'ie
 00000030  61 3a 0d 0a c2 ab 50 72  61 77 64 61 20 e2 80 94  |a:....Prawda ...|

"""
lista_znakow_specjalnych = [u'\xab', u'\x2026', u'\xbb']
for line in plik:
    # line ma typ unicode, e.g. type(line)
    for z in lista_znakow_interpunkcyjnych:
        line = line.replace(z, "")
    
    for zz in lista_znakow_specjalnych:
        line = line.replace(zz, "")
    lista = line.split()
    for s in lista:
        if s in slowa:
            slowa[s] += 1
        else:
            slowa.setdefault(s, 0)

# tu trzeba zaimplementowac top 20
i=1
for key, value in sorted(slowa.iteritems(), key=lambda (k, v):(v, k),reverse=True):
    if i>20:
        break
    print "%-10s: %-10s" % (key, value)
    i+=1
plik.close()
