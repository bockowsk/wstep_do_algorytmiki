'''
Created on Feb 18, 2018

@author: obockows
'''
from math import sqrt

while True:
    print 'uklad rownan: Ax+By=C Dx+Ey=F, podaj A, B, C, D, E, F'
    podane = raw_input("podaj A, B, C, D, E, F: ").split();
    if len(podane) != 6:
        print 'podaj 6 liczby'
        continue
    else:
        a, b, c, d, e, f=podane
    try:
        a = float(a);b = float(b);c = float(c);d = float(d);e = float(e);f = float(f)
        print 'Podane liczby:' + '\t%-5d' * 6 % (a, b, c, d, e, f)
        break
    except:
        print 'podaj liczby!'

# pierwszy uklad: Ax+By=C
rozwiazania1 = [];
delta1 = (b ** 2) - (4 * a * c);
print '%-10d' % delta1
if delta1 > 0:
    x1 = (-b - sqrt(delta1)) / (2 * a)
    x2 = (-b + sqrt(delta1)) / (2 * a)
    rozwiazania1.append(x1)
    rozwiazania1.append(x2)
elif delta1 == 0:
    x1 = (-b / (2 * a))
    rozwiazania1.append(x1)
else: 
    print 'nie ma rozwiazanie'   

# drugi uklad: Dx+Ey=F
rozwiazania2 = []
delta2 = (e ** 2) - (4 * d * f);
print delta2
if delta2 > 0:
    x1 = (-e - sqrt(delta1)) / (2 * d)
    x2 = (-e + sqrt(delta1)) / (2 * d)
    rozwiazania2.append(x1)
    rozwiazania2.append(x2)
elif delta2 == 0:
    x1 = (-e / (2 * d))
    rozwiazania2.append(x1)
else:
    print 'nie ma rozwiazania'

zbior1 = set(rozwiazania1)
print "ZBIOR 1: "
print rozwiazania1
zbior2 = set(rozwiazania2)
print "ZBIOR 2: "
print rozwiazania2
rozwiazanie = zbior1.intersection(zbior2)
# set w tuple do wyswietlenia
rozwiazanie = tuple(rozwiazanie)
print 'rozwiazania: ' + '%-5d' * len(rozwiazanie) % rozwiazanie
