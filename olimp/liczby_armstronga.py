'''
Created on Feb 19, 2018
Liczby Armstronga to N-cyfrowa liczba naturalna ktora jest suma swoich cyfr podniesionych do potegi N
Program prosi o wskazanie liczby cyfr, a nastepnie wyszukuje wszystkie Liczby Armstronga.
@author: obockows
'''
while True:
    liczba_cyfr = raw_input("podaj liczbe cyfr: ")
    if liczba_cyfr.isdigit(): break
"""
algorytm:
szuka liczby Armstroga zaczynajac od 1 do 9 powtorzone liczba_cyfr
np. dla liczba_cyft 4, szuka od 1 do 9999
"""

start = 1
stop = (int)("9"*(int)(liczba_cyfr))
print "od: {0}, do: {1}".format(start, stop)
rozwiazanie = []

for i in range(start, stop + 1):
    lista = [int(x) for x in str(i)]
    potega = len(lista)
    suma = 0;
    for j in lista:
        suma += j ** potega
    if i == suma:
        rozwiazanie.append(i)
        # print "Liczba Armstronga: "+str(i)
print 'Liczby Armstronga z przedzialu od {0} do {1}'.format(start, stop)
# list w tuple
rozwiazanie = tuple(rozwiazanie)
for liczba in rozwiazanie:
    print '%-5d'  % liczba,

