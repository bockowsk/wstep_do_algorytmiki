'''
Created on Feb 21, 2018

@author: obockows
'''


def encode_shift_letter(letter, shift):
    letter = letter.upper()
    temp = ord(letter) + shift
    if temp > 90:
        temp -= 26
    new_letter = chr(temp)
    return new_letter


szyfr = "31206"
plik1 = open('../wiadomosc.txt', 'r')
plik2 = open('../zaszyfrowana.txt', 'w')
# po liniach
for linia in plik1:
    nowa_linia = ''
    dlugosc = len(linia)
    recepta = (szyfr * dlugosc)[:dlugosc]
    krok = 0
    print linia
    print recepta
    # po literach
    linia = linia.upper()
    for l in linia:
        if ord(l) <= 90 and ord(l) >= 65:
            nowa_litera = encode_shift_letter(l, int(recepta[krok]))
            nowa_linia += nowa_litera
            krok += 1
        else:
            # tylko dodaj
            nowa_linia += l
    plik2.write(nowa_linia)
plik1.close()
plik2.close()            
    
def decode_shift_letter(letter, shift):
    letter = letter.upper()
    temp = ord(letter) - shift
    if temp < 65:
        temp += 26
    new_letter = chr(temp)
    return new_letter

plik2=open('../zaszyfrowana.txt','r')
plik3 = open('../odszyfrowana.txt', 'w')
for linia in plik2:
    nowa_linia = ''
    dlugosc = len(linia)
    recepta = (szyfr * dlugosc)[:dlugosc]
    krok = 0
    print linia
    print recepta
    # po literach
    linia = linia.upper()
    for l in linia:
        if ord(l) <= 90 and ord(l) >= 65:
            nowa_litera = decode_shift_letter(l, int(recepta[krok]))
            nowa_linia += nowa_litera
            krok += 1
        else:
            # tylko dodaj
            nowa_linia += l
    plik3.write(nowa_linia)

plik2.close()
plik3.close()
