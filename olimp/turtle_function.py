'''
Created on Feb 21, 2018

@author: obockows
'''

from turtle import Turtle, Screen

wzor=raw_input("podaj wzor funkcji: ")

WIDTH, HEIGHT = 20, 15  # coordinate system size

def plotter(turtle, x_range,wzor):
    turtle.penup()

    for x in x_range:
        #y = x / 2 + 3
        y=eval(wzor)
        ivy.goto(x, y)
        turtle.pendown()

def axis(turtle, distance, tick):
    position = turtle.position()
    turtle.pendown()

    for _ in range(0, distance // 2, tick):
        turtle.forward(tick)
        turtle.dot()

    turtle.setposition(position)

    for _ in range(0, distance // 2, tick):
        turtle.backward(tick)
        turtle.dot()

screen = Screen()
screen.setworldcoordinates(-WIDTH/2, -HEIGHT/2, WIDTH//2, HEIGHT/2)

ivy = Turtle(visible=False)
ivy.speed('fastest')
ivy.penup()
axis(ivy, WIDTH, 1)

ivy.penup()
ivy.home()
ivy.setheading(90)
axis(ivy, HEIGHT, 1)
ivy.penup()
ivy.home()
ivy.setheading(90)
axis(ivy, HEIGHT, 1)

plotter(ivy, range(-WIDTH//2, WIDTH//2),wzor)

screen.exitonclick()