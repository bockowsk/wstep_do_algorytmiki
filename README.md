Zadania do samodzielnego rozwiązania
1. Proszę napisad program rozwiązujący układ równa:
Ax+By=C
Dx+Ey=F
Współczynniki A,B,C,D,E,F należy prowadzid z klawiatury.
Program powinien uwzględnid przypadki układu nieoznaczonego i sprzecznego.
2. Liczby Armstronga to N-cyfrowa liczba naturalna która jest sumą swoich cyfr podniesionych do potęgi N. Na
przykład: 153 = 1 3 +5 3 +3 3 . Proszę napisad program znajdujący jak najwięcej takich liczb.
3. Palindrom to coś, co czyta się tak samo od przodu i od tyłu. Hipoteza: weź dowolną liczbę naturalną. Jeżeli
nie jest palindromem, to zapisz ją od tyłu i dodaj obie liczby. Jeżeli wynik nadal nie jest palindromem,
kontynuuj, traktując go jako daną. Przerwij, gdy osiągniesz palindrom. Na przykład: 78+87=165,
165+561=726, 726+627=1353, 1353+3531=4884. Napisz program sprawdzający hipotezę dla pierwszych 200
liczb naturalnych jako startowych. Czy zawsze osiągniemy palindrom?
4. Metoda Sita Eratostenesa. Ze zbioru liczb naturalnych z przedziału *2,n+, wybieramy najmniejszą, czyli 2, i
wykreślamy wszystkie jej wielokrotności większe od niej samej. Z pozostałych liczb wybieramy najmniejszą
niewykreśloną liczbę (3) i usuwamy wszystkie jej wielokrotności większe od niej samej. Według tej samej
procedury postępujemy dla kolejnych liczb. Proces ten pozostawia nieskreślone wyłącznie liczby pierwsze.
Proszę napisad program wyszukujący liczby pierwsze w zadanym zakresie.
5. Proszę napisad program, który wczytuje tekst i wypisuje 20 najczęściej występujących słów. Proszę podad
wyniki dla tekstu „Pana Tadeusza”.
6. Komputer jest doskonałym narzędziem służącym do szyfrowania i deszyfrowania tajnych wiadomości. W
metodzie Gronsfelda, będącą modyfikacją szyfru Cezara, stosuje się klucz liczbowy. Biorąc klucz o wartości
31206 i niezaszyfrowany tekst „PROGRAMOWANIE”, uzyskujemy następujący szyfrogram:
31206 31206 312
PROGR AMOWA NIE
SSQGX DNQWG QJG
Kolejne litery są przesuwane o kolejne wartości z klucza. Proszę napisad programy dokonujące szyfrowania i
deszyfrowania pliku tekstowego zadanym kluczem.
7. Używając biblioteki turtle, napisad program rysujący wykresy funkcji jednej zmiennej (na przykład: y=x*x-
6*x+3). Jako dane należy wczytad wzór funkcji oraz przedział dla zmiennej x. Wskazówka: przydatna będzie
funkcja eval.

- Należy rozwiązad minimum 3 zadania.
- Rozwiązanie zadania powinno zawierad: krótki opis rozwiązania, kod programu, wyniki programu dla
przykładowych danych.
- Rozwiązania należy umieścid w systemie modle, można też przysład na adres: mag@agh.edu.pl w temacie
wpisując słowo „python”.
